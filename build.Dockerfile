FROM python:3.11.4-alpine
LABEL maintainer="Lucky Wirasakti <lucky.wirasakti@gmail.com>"
ENV PYTHONUNBUFFERED=1
ENV USER=app
ENV HOME=/home/$USER
WORKDIR $HOME

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .
RUN python manage.py collectstatic --noinput && \
    addgroup -S $USER && adduser -S $USER -G $USER && \
    chown -R $USER:$USER $HOME 
USER $USER
EXPOSE 8000
ENTRYPOINT ["python", "manage.py", "runserver", "0.0.0.0:8000"]
FROM python:3.11.4-alpine AS base
LABEL maintainer="Lucky Wirasakti <lucky.wirasakti@gmail.com>"
ENV PYTHONUNBUFFERED=1
ENV USER=django
ENV HOME=/home/$USER
WORKDIR $HOME

FROM base AS builder
COPY requirements.txt .
RUN pip install -r requirements.txt

FROM base AS final
COPY --from=builder /usr/local/lib/python3.11/site-packages /usr/local/lib/python3.11/site-packages
COPY . .
RUN python manage.py collectstatic --noinput && \
    addgroup -S $USER && adduser -S $USER -G $USER && \
    chown -R $USER:$USER $HOME 
USER $USER
EXPOSE 8000
ENTRYPOINT ["python", "manage.py", "runserver", "0.0.0.0:8000"]